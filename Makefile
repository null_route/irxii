SOURCES = $(wildcard src/*.c)
OBJECTS = $(patsubst src/%.c,obj/%.o,$(SOURCES))
INCLUDE = -Iinclude

CFLAGS += $(INCLUDE)
CFLAGS += -std=c89
CFLAGS += -pedantic -Wall
CFLAGS += -O0
CFLAGS += -fsanitize=address
#CFLAGS += -flto=4
CFLAGS += -Wall
CFLAGS += -D_XOPEN_SOURCE
CFLAGS += -Wno-overlength-strings
CFLAGS += -Wno-unused

OUT = irxii

all: $(OBJECTS) $(OUT)

include Makefile.deps

obj/%.o: src/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(OUT): $(OBJECTS)
	$(CC) $(CFLAGS) $^ -o $(OUT)

clean:
	$(RM) obj/*.o
	$(RM) $(OUT)
