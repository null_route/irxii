#ifndef ARRAY_H
#define ARRAY_H

#include <stddef.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "safe_free.h"

#define ARRAY_NULL_CONSTRUCTOR ;
#define ARRAY_NULL_DESTRUCTOR ;
#define ARRAY_NULL_INIT_ARGS
#define ARRAY_INIT_ARGS(x) , x
#define ARRAY_STATIC static
#define ARRAY_NOT_STATIC

#define ARRAY_TEMPLATE_STRUCT(ArrayName, Data, Size, Reserve, T)              \
struct ArrayName {                                                            \
        T *Data;                                                              \
        size_t Size;                                                          \
        size_t Reserve;                                                       \
};                                                                            \

#define ARRAY_TEMPLATE_HEADER(ArrayT, ArrayName,                              \
        Data, Size, Reserve, T, InitArgs, Static)                             \
                                                                              \
Static int                                                                    \
ArrayName##_init(ArrayT *self InitArgs);                                      \
Static int                                                                    \
ArrayName##_destroy(ArrayT *self);                                            \
Static int                                                                    \
ArrayName##_grow(ArrayT *self, size_t new_size);                              \
Static int                                                                    \
ArrayName##_shrink(ArrayT *self, size_t new_size);                            \
Static int                                                                    \
ArrayName##_shrink_to_size(ArrayT *self);                                     \
Static int                                                                    \
ArrayName##_append(ArrayT *self, const T elem);                               \
Static int                                                                    \
ArrayName##_append_ptr(ArrayT *self, const T *elem);                          \
Static int                                                                    \
ArrayName##_append_array(ArrayT *self, const T *elems, size_t num_elems);     \
Static int                                                                    \
ArrayName##_insert(ArrayT *self, size_t idx, const T elem);                   \
Static int                                                                    \
ArrayName##_insert_array(ArrayT *self, size_t idx, const T *elems,            \
    size_t num_elems);                                                        \
Static int                                                                    \
ArrayName##_insert_ptr(ArrayT *self, size_t idx, T *elem);                    \
Static int                                                                    \
ArrayName##_delete(ArrayT *self, size_t idx);                                 \
Static int                                                                    \
ArrayName##_pop(ArrayT *self);                                                \
Static int                                                                    \
ArrayName##_pop_num(ArrayT *self, size_t num);                                \

#define ARRAY_TEMPLATE_CODE(ArrayT, ArrayName,                                \
        Data, Size, Reserve, T, InitArgs, Static, Constructor, Destructor)    \
                                                                              \
Static int                                                                    \
ArrayName##_init(ArrayT *self InitArgs)                                       \
{                                                                             \
        self->Data = NULL;                                                    \
        self->Size = 0;                                                       \
        self->Reserve = 0;                                                    \
        Constructor;                                                          \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_destroy(ArrayT *self)                                             \
{                                                                             \
        Destructor;                                                           \
        SAFE_FREE(self->Data);                                                \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_grow(ArrayT *self, size_t new_size)                               \
{                                                                             \
        size_t old_size = self->Size;                                         \
        size_t old_reserve;                                                   \
        size_t new_reserve;                                                   \
        size_t old_nb;                                                        \
        size_t new_nb;                                                        \
                                                                              \
        if (new_size <= old_size)                                             \
                return EOVERFLOW;                                             \
                                                                              \
        old_reserve = self->Reserve;                                          \
                                                                              \
        if (new_size >= old_reserve) {                                        \
                new_reserve = 2 * new_size;                                   \
                                                                              \
                if (new_reserve <= old_reserve)                               \
                        return EOVERFLOW;                                     \
                                                                              \
                old_nb = old_reserve * sizeof (T);                            \
                new_nb = new_reserve * sizeof (T);                            \
                                                                              \
                if (new_nb <= old_nb)                                         \
                        return EOVERFLOW;                                     \
                                                                              \
                if ((self->Data = (T*) realloc(self->Data, new_nb)) == NULL)  \
                        return ENOMEM;                                        \
                                                                              \
                self->Reserve = new_reserve;                                  \
        }                                                                     \
                                                                              \
        self->Size = new_size;                                                \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_shrink(ArrayT *self, size_t new_size)                             \
{                                                                             \
        size_t old_size = self->Size;                                         \
        size_t new_reserve;                                                   \
        size_t new_nb;                                                        \
                                                                              \
        if (new_size == old_size)                                             \
                return 0;                                                     \
        else if (new_size >= old_size)                                        \
                return EINVAL;                                                \
                                                                              \
        new_reserve = new_size;                                               \
        new_nb = new_reserve * sizeof (T);                                    \
                                                                              \
        self->Data = (T*) realloc(self->Data, new_nb);                        \
        self->Reserve = new_reserve;                                          \
        self->Size = new_size;                                                \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_shrink_to_size(ArrayT *self)                                      \
{                                                                             \
        int res;                                                              \
                                                                              \
        if ((res = ArrayName##_shrink(self, self->Size)) != 0)                \
                return res;                                                   \
                                                                              \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_append(ArrayT *self, const T elem)                                \
{                                                                             \
        int res;                                                              \
        size_t old_size = self->Size;                                         \
        size_t new_size = old_size + 1;                                       \
                                                                              \
        if ((res = ArrayName##_grow(self, new_size)) != 0)                    \
                return res;                                                   \
                                                                              \
        self->Data[old_size] = elem;                                          \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_append_ptr(ArrayT *self, const T *elem)                           \
{                                                                             \
        int res;                                                              \
        size_t old_size = self->Size;                                         \
        size_t new_size = old_size + 1;                                       \
                                                                              \
        if ((res = ArrayName##_grow(self, new_size)) != 0)                    \
                return res;                                                   \
                                                                              \
        self->Data[old_size] = *elem;                                         \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_append_array(ArrayT *self, const T *elems, size_t num_elems)      \
{                                                                             \
        int res;                                                              \
        size_t old_size = self->Size;                                         \
        size_t new_size = old_size + num_elems;                               \
                                                                              \
        if ((res = ArrayName##_grow(self, new_size)) != 0)                    \
                return res;                                                   \
                                                                              \
        memcpy(&self->Data[old_size], (void*) elems, num_elems * sizeof (T)); \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_insert(ArrayT *self, size_t idx, const T elem)                    \
{                                                                             \
        int res;                                                              \
        size_t old_size = self->Size;                                         \
        size_t new_size = old_size + 1;                                       \
        size_t num_after;                                                     \
                                                                              \
        if (idx > old_size)                                                   \
                return EOVERFLOW;                                             \
                                                                              \
        if ((res = ArrayName##_grow(self, new_size)) != 0)                    \
                return res;                                                   \
                                                                              \
        num_after = old_size - idx;                                           \
                                                                              \
        if (num_after > 0)                                                    \
                memmove(&self->Data[idx + 1], &self->Data[idx], num_after *   \
                    sizeof (T));                                              \
                                                                              \
        self->Data[idx] = elem;                                               \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_insert_ptr(ArrayT *self, size_t idx, T *elem)                     \
{                                                                             \
        int res;                                                              \
        size_t old_size = self->Size;                                         \
        size_t new_size = old_size + 1;                                       \
        size_t num_after;                                                     \
                                                                              \
        if (idx > old_size)                                                   \
                return EOVERFLOW;                                             \
                                                                              \
        if ((res = ArrayName##_grow(self, new_size)) != 0)                    \
                return res;                                                   \
                                                                              \
        num_after = old_size - idx;                                           \
                                                                              \
        if (num_after > 0)                                                    \
                memmove(&self->Data[idx + 1], &self->Data[idx], num_after *   \
                    sizeof (T));                                              \
                                                                              \
        self->Data[idx] = *elem;                                              \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_insert_array(ArrayT *self, size_t idx, const T *elems,            \
    size_t num_elems)                                                         \
{                                                                             \
        int res;                                                              \
        size_t old_size = self->Size;                                         \
        size_t new_size = old_size + num_elems;                               \
        size_t num_after;                                                     \
                                                                              \
        if (idx > old_size)                                                   \
                return EOVERFLOW;                                             \
                                                                              \
        if ((res = ArrayName##_grow(self, new_size)) != 0)                    \
                return res;                                                   \
                                                                              \
        num_after = old_size - idx;                                           \
                                                                              \
        if (num_after > 0) {                                                  \
                memmove(&self->Data[idx + num_elems], &self->Data[idx],       \
                    num_after * sizeof (T));                                  \
        }                                                                     \
                                                                              \
        memcpy(&self->Data[idx], (void*) elems, num_elems);                   \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_delete(ArrayT *self, size_t idx)                                  \
{                                                                             \
        size_t old_size = self->Size;                                         \
        size_t new_size;                                                      \
        size_t num_after;                                                     \
                                                                              \
        if (idx >= old_size)                                                  \
                return EINVAL;                                                \
                                                                              \
        new_size = old_size - 1;                                              \
        num_after = old_size - idx;                                           \
        memmove(&self->Data[idx], &self->Data[idx + 1],                       \
            num_after * sizeof (T));                                          \
        self->Size = new_size;                                                \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_pop(ArrayT *self)                                                 \
{                                                                             \
        if (self->Size == 0)                                                  \
                return EINVAL;                                                \
                                                                              \
        self->Size--;                                                         \
        return 0;                                                             \
}                                                                             \
                                                                              \
Static int                                                                    \
ArrayName##_pop_num(ArrayT *self, size_t num)                                 \
{                                                                             \
        size_t size = self->Size;                                             \
                                                                              \
        if (size == 0)                                                        \
                return 0;                                                     \
                                                                              \
        if (num >= size)                                                      \
                self->Size = 0;                                               \
        else                                                                  \
                self->Size -= num;                                            \
                                                                              \
        return 0;                                                             \
}                                                                             \

#endif
