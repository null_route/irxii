#ifndef MESSAGE_H
#define MESSAGE_H

#include "str.h"
#include <stddef.h>
#include <time.h>

enum message_type {
	MESSAGE_NORMAL,
	MESSAGE_RAW,
	MESSAGE_INFO,
	MESSAGE_ERROR
};

struct message {
	struct str msg;
	enum message_type type;
	size_t len;
	time_t timestamp;
};

void
message_init(struct message *self, enum message_type type, const char *s);
void
message_destroy(struct message *self);
enum message_type
message_type(struct message *self);

#endif
