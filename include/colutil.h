#ifndef COLUTIL_H
#define COLUTIL_H

#include <stddef.h>
#include "utf8.h"

size_t
colutil_bytes_until(const char *s, size_t col);

#if 0
size_t
colutil_num_cols(const char *s);
#endif

#endif
