#ifndef TIMESTAMP_H
#define TIMESTAMP_H

#include "str.h"
#include <time.h>
#include <stddef.h>

int
timestamp_append(struct str *s, size_t *dst_len, time_t time);

#endif
