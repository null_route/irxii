#ifndef IRC_SERVER_LIST_H
#define IRC_SERVER_LIST_H

#include <stddef.h>
#include "irc_server.h"

extern struct irc_server_list {
	struct irc_server *servers;
	size_t num_servers;
} irc_server_list;

void
irc_server_list_init(struct irc_server_list *self);
void
irc_server_list_destroy(struct irc_server_list *self);
struct irc_server *
irc_server_list_add(struct irc_server_list *self, const char *host, unsigned short port, const char *chatnet);

#endif
