#ifndef IRC_WINDOW_SERVER_MAP_H
#define IRC_WINDOW_SERVER_MAP_H

/* TODO come up with a better name for this */

#include "bool.h"
#include "window.h"
#include "irc_server.h"
#include <stddef.h>

extern struct irc_window_map {
	struct window **windows;
	struct irc_server **servers;
	size_t num_entries;
} irc_window_map;

void
irc_window_map_init(struct irc_window_map *self);
void
irc_window_map_destroy(struct irc_window_map *self);
bool_t
irc_window_map_add_entry(struct irc_window_map *self, struct irc_server *server, struct window *window);
struct window *
irc_window_map_lookup_window(struct irc_window_map *self, struct irc_server *server);
struct irc_server *
irc_window_map_lookup_server(struct irc_window_map *self, struct window *window);

#endif
