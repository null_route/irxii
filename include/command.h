#ifndef COMMAND_H
#define COMMAND_H

#include "str.h"
#include "bool.h"

typedef void (*command_callback_t)(void);

struct command {
	struct str command;
	command_callback_t callback;
};

void
command_init(struct command *self, const char *command, command_callback_t callback);
void
command_destroy(struct command *self);

#endif
