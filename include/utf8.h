#ifndef UTF8_H
#define UTF8_H

#include <stddef.h>

size_t
utf8_chr_num_bytes(char ch);
int
utf8_is_valid_continuation(char ch);
int
utf8_is_valid_chrseq(const char *utf8_chr, size_t num_bytes);
size_t
utf8_chrseq_num_cols(const char *utf8_chr);

#endif
