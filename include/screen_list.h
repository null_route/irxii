#ifndef SCREEN_LIST_H
#define SCREEN_LIST_H

#include <stddef.h>
#include "screen.h"
#include "window.h"
#include "bool.h"

/* has to be global so src/signals.c can signal a redraw */

extern struct screen_list {
	struct screen *screens;
	size_t size;
	size_t reserve;
	size_t focused;
} screen_list;

void
screen_list_init(struct screen_list *self);
void
screen_list_destroy(struct screen_list *self);
size_t
screen_list_get_num_screens(struct screen_list *self);
struct screen *
screen_list_screen_list(struct screen_list *self);
struct screen *
screen_list_get_focused_screen(struct screen_list *self);
struct screen *
screen_list_add_screen(struct screen_list *self);
void
screen_list_set_focused_screen(struct screen_list *self, size_t index);
void
screen_list_update_size(struct screen_list *self);
bool_t
screen_list_delete_screen(struct screen_list *self, size_t i);
struct window *
screen_list_focused_window(struct screen_list *self);

#endif
