#ifndef TERM_H
#define TERM_H

#include <stddef.h>

int
term_init(void);
void
term_reset(void);
void
term_update_size(void);
size_t
term_get_num_rows(void);
size_t
term_get_num_cols(void);

#endif
