#ifndef NET_H
#define NET_H

typedef int net_socket_t;

#define NET_SOCKET_INVALID -1

net_socket_t
net_connect(const char *host, unsigned short port);

#endif
