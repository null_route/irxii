#ifndef STR_H
#define STR_H

#include <stddef.h>
#include "array.h"

struct str {
	char *data;        /* raw byte array - don't use this directly */
	size_t size;     /* used memory */
	size_t reserved;   /* reserved memory */
};

#define STR(self) ((const char*) (self).data)
#define STR_LEN(self) ((self)->size > 0 ? (self)->size - 1 : 0)

int
str_init(struct str *self);
int
str_init_with(struct str *self, const char *s);
void
str_destroy(struct str *self);
int
str_append_chr(struct str *self, char ch);
int
str_append(struct str *self, const char *utf8_str);
int
str_append_len(struct str *self, const char *utf8_str, size_t byte_offset);
const char *
str_idx(struct str *self, size_t idx, char *out_ch, size_t out_ch_size);
void
str_del_at(struct str *self, size_t idx);

#endif
