#ifndef COMMAND_BUILTINS_H
#define COMMAND_BUILTINS_H

#include "command_list.h"

void
command_builtins_add(struct command_list *list);

#endif
