#ifndef COMMAND_LIST_H
#define COMMAND_LIST_H

#include <stddef.h>
#include "command.h"

extern struct command_list {
	struct command *commands;
	size_t num_commands;
} command_list;

void
command_list_process_line(struct command_list *self, const char *line);
void
command_list_add_command(struct command_list *self, const char *command, command_callback_t callback);
struct command *
command_list_get_command(struct command_list *self, const char *command);
bool_t
command_list_has_command(struct command_list *self, const char *command);
bool_t
command_list_del_command(struct command_list *self, const char *command);
void
command_list_init(struct command_list *self);
void
command_list_destroy(struct command_list *self);

#endif
