#ifndef DRAW_H
#define DRAW_H

#include "ansi.h"
#include "screen.h"
#include "screen_list.h"
#include "window.h"

void
draw_set_secondary_screen(void);
void
draw_set_primary_screen(void);
void
draw_redraw_focused_screen(void);
void
draw_redraw_screen(struct screen *screen);
void
draw_redraw_backlog_if_needed(struct window *window);
void
draw_set_color(enum ansi_color color);
void
draw_set_pos(unsigned row, unsigned col);
void
draw_reset_style(void);
void
draw_clear_history(void);
void
draw_clear_screen(void);
void
draw_clear_line(void);
void
draw_clear_line_to_end(void);
void
draw_clear_line_to_start(void);
void
draw_reset_col(void);
void
draw_next_line_keep_col(void);
void
draw_next_line(void);
void
draw_set_secondary_screen(void);
void
draw_set_primary_screen(void);
void
draw_reset(void);
void
draw_write(const char *s);
void
draw_write_len(const char *s, size_t len);

#endif
