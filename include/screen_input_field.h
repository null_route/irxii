#ifndef SCREEN_INPUT_FIELD_H
#define SCREEN_INPUT_FIELD_H

#include "str.h"
#include <stddef.h>

/* TODO track caret positions */

struct screen_input_field {
	struct str input;
};

void
screen_input_field_init(struct screen_input_field *self);
void
screen_input_field_destroy(struct screen_input_field *self);
void
screen_input_field_render(struct screen_input_field *self);
void
screen_input_field_append_chr(struct screen_input_field *self, char ch);
void
screen_input_field_delete_char(struct screen_input_field *self);
void
screen_input_field_flush(struct screen_input_field *self);
const char *
screen_input_field_get_content(struct screen_input_field *self);

#endif
