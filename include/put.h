#ifndef PUT_H
#define PUT_H

#include "window.h"
#include <stdarg.h>

void
put_message_fmt(struct window *window, const char *fmt, ...);
void
put_info_fmt(struct window *window, const char *fmt, ...);
void
put_error_fmt(struct window *window, const char *fmt, ...);
void
put_raw_fmt(struct window *window, const char *fmt, ...);
void
put_message(struct window *window, const char *s);
void
put_info(struct window *window, const char *s);
void
put_error(struct window *window, const char *s);
void
put_raw(struct window *window, const char *s);

#endif
