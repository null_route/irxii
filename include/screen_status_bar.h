#ifndef SCREEN_STATUS_BAR_H
#define SCREEN_STATUS_BAR_H

#include "str.h"
#include <stddef.h>

struct screen_status_bar {
	struct str status;
	size_t num_rows;
};

void
screen_status_bar_init(struct screen_status_bar *self, size_t num_rows, const char *status);
void
screen_status_bar_destroy(struct screen_status_bar *self);
void
screen_status_bar_resize(struct screen_status_bar *self, size_t num_rows);
void
screen_status_bar_render(struct screen_status_bar *self);

#endif
