#ifndef FD_REACTOR_H
#define FD_REACTOR_H

#include <stddef.h>
#include <poll.h>
#include "array.h"
#include "bool.h"

typedef void (*fd_reactor_callback_fn_t)(short revents, void *arg);

struct fd_reactor_callback {
	int fd;
	fd_reactor_callback_fn_t callback;
	void *callback_args;
};

ARRAY_TEMPLATE_STRUCT(
	fd_reactor_callback_vec,
	data,
	size,
	reserve,
	struct fd_reactor_callback
)

ARRAY_TEMPLATE_HEADER(
	struct fd_reactor_callback_vec,
	fd_reactor_callback_vec,
	data,
	size,
	reserve,
	struct fd_reactor_callback,
	ARRAY_NULL_INIT_ARGS,
	ARRAY_NOT_STATIC
)

ARRAY_TEMPLATE_STRUCT(
	fd_reactor_pollfd_vec,
	data,
	size,
	reserve,
	struct pollfd
)

ARRAY_TEMPLATE_HEADER(
	struct fd_reactor_pollfd_vec,
	fd_reactor_pollfd_vec,
	data,
	size,
	reserve,
	struct pollfd,
	ARRAY_NULL_INIT_ARGS,
	ARRAY_NOT_STATIC
)

extern struct fd_reactor {
	struct fd_reactor_pollfd_vec pollfds;
	struct fd_reactor_callback_vec callbacks;
} fd_reactor;

void
fd_reactor_init(struct fd_reactor *self);
void
fd_reactor_destroy(struct fd_reactor *self);
bool_t
fd_reactor_has_fd(struct fd_reactor *self, int fd);
void
fd_reactor_del_callback(struct fd_reactor *self, int fd);
bool_t
fd_reactor_update(struct fd_reactor *self);
bool_t
fd_reactor_add_fd(struct fd_reactor *self, int fd, short events);
bool_t
fd_reactor_add_callback(struct fd_reactor *self, int fd,
	fd_reactor_callback_fn_t callback, void *callback_args);
void
fd_reactor_del_fd(struct fd_reactor *self, int fd);

#endif
