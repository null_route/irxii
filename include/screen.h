#ifndef SCREEN_H
#define SCREEN_H

#include "screen_input_field.h"
#include "screen_status_bar.h"
#include "window.h"
#include "bool.h"

struct screen {
	size_t num_windows;
	struct window *windows;
	struct screen_input_field input_field;
	struct screen_status_bar status_bar;
};

void
screen_init(struct screen *self);
void
screen_destroy(struct screen *self);
struct window *
screen_add_window(struct screen *self, const char *title);
bool_t
screen_delete_window(struct screen *self, size_t i);

#endif
