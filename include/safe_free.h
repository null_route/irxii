#ifndef SAFE_FREE_H
#define SAFE_FREE_H

#include <stdlib.h>

#define SAFE_FREE(x) \
	do { if ((x)) { free((x)); (x) = NULL; } } while (0);

#endif
