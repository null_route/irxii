#ifndef IRC_SERVER_H
#define IRC_SERVER_H

#include "async_socket.h"
#include "str.h"

struct irc_server {
	struct str host;
	struct str chatnet;
	unsigned short port;
	struct async_socket sock;
};

void
irc_server_init(struct irc_server *self, const char *host, unsigned short port, const char *chatnet);
void
irc_server_destroy(struct irc_server *self);
bool_t
irc_server_connect(struct irc_server *self, struct fd_reactor *reactor);

#endif
