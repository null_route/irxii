#ifndef STARTUP_LOGO_H
#define STARTUP_LOGO_H

#include "window.h"

void
startup_logo_write(struct window *window);

#endif
