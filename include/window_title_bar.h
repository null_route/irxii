#ifndef WINDOW_TITLE_BAR_H
#define WINDOW_TITLE_BAR_H

#include "str.h"
#include <stddef.h>

struct window_title_bar {
	struct str title;
	size_t num_rows;
};

void
window_title_bar_init(struct window_title_bar *self, size_t num_rows, const char *title);
void
window_title_bar_destroy(struct window_title_bar *self);
void
window_title_bar_render(struct window_title_bar *self);
void
window_title_bar_resize(struct window_title_bar *self, size_t num_rows);

#endif
