#ifndef ASYNC_SOCKET_H
#define ASYNC_SOCKET_H

#include "bool.h"
#include "fd_reactor.h"
#include <stddef.h>

struct async_socket {
	int sock;
	fd_reactor_callback_fn_t callback;
	char buf[513];
	size_t buf_len;
};

void
async_socket_init(struct async_socket *self);
void
async_socket_destroy(struct async_socket *self);
bool_t
async_socket_connect(struct async_socket *self, struct fd_reactor *reactor, const char *host,
	unsigned short port, short events, fd_reactor_callback_fn_t callback, void *arg);
bool_t
async_socket_close(struct async_socket *self, struct fd_reactor *reactor);
void
async_socket_set_callback(struct async_socket *self, fd_reactor_callback_fn_t callback);
void
async_socket_recv(struct async_socket *self);

#endif
