#ifndef BOOL_H
#define BOOL_H

enum { BOOL_FALSE, BOOL_TRUE };
typedef unsigned char bool_t;

#endif
