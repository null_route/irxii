#ifndef STRBUF_H
#define STRBUF_H

#include <limits.h>

#define STRBUF_STRINGIFY(x)  #x
#define STRBUF_LEN(x)        (sizeof x - 1)
#define STRBUF_NUM_LEN(x)    (STRBUF_LEN(STRBUF_STRINGIFY(x)))

#define STRBUF_INT_MAX       STRBUF_NUM_LEN(INT_MAX)
#define STRBUF_UINT_MAX      STRBUF_NUM_LEN(UINT_MAX)

#endif
