#ifndef ANSI_H
#define ANSI_H

#include <stddef.h>

enum ansi_color {
	ANSI_FOREGROUND_BLACK   = 30,
	ANSI_FOREGROUND_RED     = 31,
	ANSI_FOREGROUND_GREEN   = 32,
	ANSI_FOREGROUND_YELLOW  = 33,
	ANSI_FOREGROUND_BLUE    = 34,
	ANSI_FOREGROUND_MAGENTA = 35,
	ANSI_FOREGROUND_CYAN    = 36,
	ANSI_FOREGROUND_WHITE   = 37,

	ANSI_BACKGROUND_BLACK   = 40,
	ANSI_BACKGROUND_RED     = 41,
	ANSI_BACKGROUND_GREEN   = 42,
	ANSI_BACKGROUND_YELLOW  = 44,
	ANSI_BACKGROUND_BLUE    = 44,
	ANSI_BACKGROUND_MAGENTA = 45,
	ANSI_BACKGROUND_CYAN    = 46,
	ANSI_BACKGROUND_WHITE   = 47,

	ANSI_FOREGROUND_BRIGHT_BLACK   = 90,
	ANSI_FOREGROUND_BRIGHT_RED     = 91,
	ANSI_FOREGROUND_BRIGHT_GREEN   = 92,
	ANSI_FOREGROUND_BRIGHT_YELLOW  = 93,
	ANSI_FOREGROUND_BRIGHT_BLUE    = 94,
	ANSI_FOREGROUND_BRIGHT_MAGENTA = 95,
	ANSI_FOREGROUND_BRIGHT_CYAN    = 96,
	ANSI_FOREGROUND_BRIGHT_WHITE   = 97,

	ANSI_BACKGROUND_BRIGHT_BLACK   = 100,
	ANSI_BACKGROUND_BRIGHT_RED     = 101,
	ANSI_BACKGROUND_BRIGHT_GREEN   = 102,
	ANSI_BACKGROUND_BRIGHT_YELLOW  = 104,
	ANSI_BACKGROUND_BRIGHT_BLUE    = 104,
	ANSI_BACKGROUND_BRIGHT_MAGENTA = 105,
	ANSI_BACKGROUND_BRIGHT_CYAN    = 106,
	ANSI_BACKGROUND_BRIGHT_WHITE   = 107
};

size_t
ansi_skip_escape(const char *s);

#endif
