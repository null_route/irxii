#ifndef WINDOW_H
#define WINDOW_H

#include "message.h"
#include "window_title_bar.h"

struct window {
	struct window_title_bar title_bar;
	struct message *messages;
	size_t num_messages;
};

void
window_init(struct window *self, const char *title);
void
window_destroy(struct window *self);
void
window_write(struct window *self, enum message_type type, const char *s);

#endif
