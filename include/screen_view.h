#ifndef SCREEN_VIEW_H
#define SCREEN_VIEW_H

#include <stddef.h>
#include "screen.h"
#include "window_view.h"

struct screen_view {
	struct screen *screen;
	struct window_view window_view;
	size_t num_rows;
	size_t num_cols;
	size_t focused_window_idx;
} screen_view;

void
screen_view_init(struct screen_view *self, size_t num_rows, size_t num_cols);
void
screen_view_focus(struct screen_view *self, struct screen *screen);
void
screen_view_focus_window(struct screen_view *self, size_t idx);

#endif
