#ifndef WINDOW_VIEW_H
#define WINDOW_VIEW_H

#include <stddef.h>
#include "window.h"

struct window_view {
	struct window *window;
	size_t num_rows;
	size_t num_cols;
};

void
window_view_init(struct window_view *self, size_t num_rows, size_t num_cols);
void
window_view_focus(struct window_view *self, struct window *window);

#endif
