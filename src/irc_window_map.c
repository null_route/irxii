#include "irc_window_map.h"
#include "safe_free.h"

struct irc_window_map irc_window_map;

void
irc_window_map_init(struct irc_window_map *self)
{
	self->windows = NULL;
	self->servers = NULL;
	self->num_entries = 0;
}

void
irc_window_map_destroy(struct irc_window_map *self)
{
	SAFE_FREE(self->windows);
	SAFE_FREE(self->servers);
	self->num_entries = 0;
}

bool_t
irc_window_map_add_entry(struct irc_window_map *self, struct irc_server *server, struct window *window)
{
	size_t num = self->num_entries;
	size_t num_windows = num;
	size_t num_servers = num;

	self->windows = mem_append(self->windows, &num_windows, &window, sizeof window);
	self->servers = mem_append(self->servers, &num_servers, &server, sizeof server);

	if (num_windows != num_servers) {
		/* neither of those has been propagated anywhere outside of this function
		 * the next call to mem_append will simply use the old size, resulting in
		 * a realloc that will use the old size */
		return BOOL_FALSE;
	}

	self->num_entries = num_windows; /* arbitrary, either could work */
	return BOOL_TRUE;
}

struct window *
irc_window_map_lookup_window(struct irc_window_map *self, struct irc_server *server)
{
	size_t i;

	for (i = 0; i < self->num_entries; ++i) {
		if (self->servers[i] == server)
			return self->windows[i];
	}

	return NULL;
}

struct irc_server *
irc_window_map_lookup_server(struct irc_window_map *self, struct window *window)
{
	size_t i;

	for (i = 0; i < self->num_entries; ++i) {
		if (self->windows[i] == window)
			return self->servers[i];
	}

	return NULL;
}
