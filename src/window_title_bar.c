#include "window_title_bar.h"
#include "safe_free.h"
#include "str.h"
#include "draw.h"
#include "colutil.h"

void
window_title_bar_init(struct window_title_bar *self, size_t num_rows, const char *title)
{
	self->num_rows = num_rows;
	str_init_with(&self->title, title);
}

void
window_title_bar_destroy(struct window_title_bar *self)
{
	self->num_rows = 0;
	str_destroy(&self->title);
}

void
window_title_bar_render(struct window_title_bar *self)
{
}

void
window_title_bar_resize(struct window_title_bar *self, size_t num_rows)
{
	self->num_rows = num_rows;
}
