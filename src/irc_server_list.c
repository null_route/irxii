#include "irc_server_list.h"
#include "safe_free.h"

struct irc_server_list irc_server_list;

void
irc_server_list_init(struct irc_server_list *self)
{
	self->servers = NULL;
	self->num_servers = 0;
}

void
irc_server_list_destroy(struct irc_server_list *self)
{
	size_t i;

	for (i = 0; i < self->num_servers; ++i)
		irc_server_destroy(&self->servers[i]);

	SAFE_FREE(self->servers);
	self->num_servers = 0;
}

struct irc_server *
irc_server_list_add(struct irc_server_list *self, const char *host, unsigned short port, const char *chatnet)
{
	struct irc_server server;

	irc_server_init(&server, host, port, chatnet);

	self->servers = (struct irc_server*)
		mem_append(self->servers, &self->num_servers, &server, sizeof server);

	/* TODO error check here */
	return &self->servers[self->num_servers - 1];
}
