#include "fd_reactor.h"
#include "safe_free.h"
#include "array.h"
#include "errno.h"
#include <string.h>
#include <stdio.h>

ARRAY_TEMPLATE_CODE(
	struct fd_reactor_callback_vec,
	fd_reactor_callback_vec,
	data,
	size,
	reserve,
	struct fd_reactor_callback,
	ARRAY_NULL_INIT_ARGS,
	ARRAY_NOT_STATIC,
	ARRAY_NULL_CONSTRUCTOR,
	ARRAY_NULL_DESTRUCTOR
)

ARRAY_TEMPLATE_CODE(
	struct fd_reactor_pollfd_vec,
	fd_reactor_pollfd_vec,
	data,
	size,
	reserve,
	struct pollfd,
	ARRAY_NULL_INIT_ARGS,
	ARRAY_NOT_STATIC,
	ARRAY_NULL_CONSTRUCTOR,
	ARRAY_NULL_DESTRUCTOR
)

struct fd_reactor fd_reactor;

void
fd_reactor_init(struct fd_reactor *self)
{
	fd_reactor_callback_vec_init(&self->callbacks);
	fd_reactor_pollfd_vec_init(&self->pollfds);
}

void
fd_reactor_destroy(struct fd_reactor *self)
{
	fd_reactor_callback_vec_destroy(&self->callbacks);
	fd_reactor_pollfd_vec_destroy(&self->pollfds);
}

bool_t
fd_reactor_has_fd(struct fd_reactor *self, int fd)
{
	size_t i;

	for (i = 0; i < self->pollfds.size; ++i) {
		if (self->pollfds.data[i].fd == fd)
			return BOOL_TRUE;
	}

	return BOOL_FALSE;
}

bool_t
fd_reactor_add_fd(struct fd_reactor *self, int fd, short events)
{
	struct pollfd pfd = {0};

	pfd.fd = fd;
	pfd.events = events;
	return fd_reactor_pollfd_vec_append_ptr(&self->pollfds, &pfd) == 0;
}

void
fd_reactor_callback_init(struct fd_reactor_callback *self, int fd,
	fd_reactor_callback_fn_t callback, void *callback_args)
{
	self->fd = fd;
	self->callback = callback;
	self->callback_args = callback_args;
}

bool_t
fd_reactor_add_callback(struct fd_reactor *self, int fd,
	fd_reactor_callback_fn_t callback, void *callback_args)
{
	struct fd_reactor_callback cb = {0};

	fd_reactor_callback_init(&cb, fd, callback, callback_args);
	return fd_reactor_callback_vec_append_ptr(&self->callbacks, &cb) == 0;
}

void
fd_reactor_del_fd(struct fd_reactor *self, int fd)
{
	size_t i = 0;
	struct fd_reactor_callback *callback;

	while (i < self->callbacks.size) {
		while (1) {
			callback = &self->callbacks.data[i];

			if (callback->fd != fd)
				break;

			fd_reactor_callback_vec_delete(&self->callbacks, i);
		}

		i++;
	}
}

static void
process_event(struct fd_reactor *self, short revents, int fd)
{
	size_t i;
	struct fd_reactor_callback *callback;

	for (i = 0; i < self->callbacks.size; ++i) {
		callback = &self->callbacks.data[i];

		if (callback->fd == fd)
			callback->callback(revents, callback->callback_args);
	}
}

bool_t
fd_reactor_update(struct fd_reactor *self)
{
	size_t i;
	short revents;

	if (self->pollfds.size == 0)
		return BOOL_TRUE;

	if (poll(self->pollfds.data, self->pollfds.size, -1) == -1) {
		/* TODO report error in the irc client itself here */
		if (errno != EINTR)
			perror("fd_reactor_update(): poll failed");

		return BOOL_FALSE;
	}

	for (i = 0; i < self->pollfds.size; ++i) {
		revents = self->pollfds.data[i].revents;

		if (revents != 0) {
			process_event(self, revents, self->pollfds.data[i].fd);
			self->pollfds.data[i].revents = 0;
		}
	}

	return BOOL_TRUE;
}
