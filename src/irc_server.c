#include "irc_server.h"
#include "str.h"
#include "put.h"
#include "safe_free.h"
#include "irc_window_map.h"
#include <stdio.h>

static void
irc_server_sock_callback(short revents, void *arg)
{
	struct irc_server *self = arg;
	struct window *window;

	if ((window = irc_window_map_lookup_window(
	    &irc_window_map, self)) == NULL) {
		return;
	}

	async_socket_recv(&self->sock);
}

void
irc_server_init(struct irc_server *self, const char *host, unsigned short port, const char *chatnet)
{
	async_socket_init(&self->sock);
	str_init_with(&self->host, host);
	self->port = port;
	str_init_with(&self->chatnet, chatnet);
}

void
irc_server_destroy(struct irc_server *self)
{
	str_destroy(&self->host);
	self->port = 0;
	str_destroy(&self->chatnet);
}

bool_t
irc_server_connect(struct irc_server *self, struct fd_reactor *reactor)
{
	return async_socket_connect(&self->sock, reactor, STR(self->host), self->port,
		POLLIN | POLLERR | POLLHUP, irc_server_sock_callback, self);
}
