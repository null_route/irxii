#include "put.h"
#include <malloc.h>

void
put_message(struct window *window, const char *s)
{
	window_write(window, MESSAGE_NORMAL, s);
}

void
put_info(struct window *window, const char *s)
{
	window_write(window, MESSAGE_INFO, s);
}

void
put_error(struct window *window, const char *s)
{
	window_write(window, MESSAGE_ERROR, s);
}

void
put_raw(struct window *window, const char *s)
{
	window_write(window, MESSAGE_RAW, s);
}
