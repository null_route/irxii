#include "window_view.h"

void
window_view_init(struct window_view *self, size_t num_rows, size_t num_cols)
{
	self->window = NULL;
	self->num_rows = num_rows;
	self->num_cols = num_cols;
}

void
window_view_focus(struct window_view *self, struct window *window)
{
	self->window = window;
}

void
window_view_resize(struct window_view *self, size_t num_rows, size_t num_cols)
{
	self->num_rows = 0;
	self->num_cols = 0;
}
