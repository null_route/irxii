#include "screen_status_bar.h"
#include "colutil.h"
#include "term.h"
#include "draw.h"

void
screen_status_bar_init(struct screen_status_bar *self, size_t num_rows, const char *status)
{
	str_init_with(&self->status, status);
	self->num_rows = num_rows;
}

void
screen_status_bar_destroy(struct screen_status_bar *self)
{
	str_destroy(&self->status);
}

void
screen_status_bar_resize(struct screen_status_bar *self, size_t num_rows)
{
	self->num_rows = num_rows;
}

void
screen_status_bar_render(struct screen_status_bar *self)
{
}
