#include "term.h"
#include "error.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <stdarg.h>
#include <termios.h>
#include <stdio.h>

static struct {
	struct winsize term_size;
	struct termios term_state;
	struct termios term_orig_state;
} state;

void
term_reset(void)
{
	tcsetattr(0, TCSANOW, &state.term_orig_state);
}

static void
term_update_attr(void)
{
	tcsetattr(0, TCSANOW, &state.term_state);
}

static void
term_turn_off_echo(void)
{
	state.term_state.c_lflag &= ~ICANON;
	state.term_state.c_lflag &= ~ECHO;
	state.term_state.c_lflag &= ~ISIG;
	state.term_state.c_lflag &= ~NOFLSH;
	state.term_state.c_oflag &= ~OPOST;
	state.term_state.c_oflag &= OFILL;
	state.term_state.c_iflag &= IXON;
	term_update_attr();
}

static void
term_save_orig_attrs(void)
{
	memcpy(&state.term_state, &state.term_orig_state,
		sizeof state.term_state);
}

void
term_update_size(void)
{
	ioctl(0, TIOCGWINSZ, &state.term_size);
}

int
term_init(void)
{
	if (tcgetattr(0, &state.term_orig_state) == -1)
		return 0;

	term_save_orig_attrs();
	term_turn_off_echo();
	term_update_size();
	return 1;
}

size_t
term_get_num_rows(void)
{
	return state.term_size.ws_row;
}

size_t
term_get_num_cols(void)
{
	return state.term_size.ws_col;
}
