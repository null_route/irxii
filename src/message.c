#include "message.h"
#include "safe_free.h"
#include "str.h"
#include "timestamp.h"
#include <string.h>

static void
message_init_normal(struct message *self, const char *s)
{
	timestamp_append(&self->msg, &self->len, self->timestamp);
	str_append(&self->msg, s);
}

static void
message_init_raw(struct message *self, const char *s)
{
	str_append(&self->msg, s);
}

static void
message_init_info(struct message *self, const char *s)
{
	timestamp_append(&self->msg, &self->len, self->timestamp);
	str_append(&self->msg, s);
}

static void
message_init_error(struct message *self, const char *s)
{
	timestamp_append(&self->msg, &self->len, self->timestamp);
	str_append(&self->msg, s);
}

void
message_init(struct message *self, enum message_type type, const char *s)
{
	str_init(&self->msg);
	self->len = 0;
	self->timestamp = time(NULL);

	switch ((self->type = type)) {
	case MESSAGE_RAW:
		message_init_raw(self, s);
		break;
	case MESSAGE_INFO:
		message_init_info(self, s);
		break;
	case MESSAGE_ERROR:
		message_init_error(self, s);
		break;
	default:
		message_init_normal(self, s);
		break;
	}
}

void
message_destroy(struct message *self)
{
	str_destroy(&self->msg);
	self->len = 0;
	self->timestamp = 0;
}

enum message_type
message_type(struct message *self)
{
	return self->type;
}
