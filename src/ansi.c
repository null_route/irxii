#include "ansi.h"
#include "str.h"
#include <stdio.h>

/* TODO: this is absolute garbage */
size_t
ansi_skip_escape(const char *s)
{
	const char *p = s;
	size_t i = 0;

	if (p[i] != '\x1B')
		goto ret;

	i++;

	if (!(p[i] >= 0x40 && p[i] <= 0x5F))
		goto ret;
	else
		i++;

	while (p[i] >= 0x30 && p[i] <= 0x3F)
		i++;

	while (p[i] >= 0x20 && p[i] <= 0x2F)
		i++;

	if (!(p[i] >= 0x40 && p[i] <= 0x7E))
		goto ret;
	else
		i++;

ret:
	return i;
}
