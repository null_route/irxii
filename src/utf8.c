#include "utf8.h"
#include <wchar.h>

size_t
utf8_chr_num_bytes(char ch)
{
	if ((ch & 0xE0) == 0xC0)
		return 2;
	else if ((ch & 0xF0) == 0xE0) 
		return 3;
	else if ((ch & 0xF8) == 0xF0) 
		return 4;

	return 1;
}

int
utf8_is_valid_continuation(char ch)
{
	return ((ch & 0xC0) == 0x80);
}

int
utf8_is_valid_chrseq(const char *utf8_chr, size_t num_bytes)
{
	const char *p;
	const char *max;

	if (num_bytes == 1)
		return 1;

	p = utf8_chr + 1; /* skip over first byte */
	max = utf8_chr + num_bytes;

	while (p < max) {
		if (!utf8_is_valid_continuation(*p++))
			return 0;
	}

	return 1;
}

size_t
utf8_chrseq_num_cols(const char *utf8_chr)
{
	if (*utf8_chr == '\0')
		return 0;

	/* TODO */
	return 1;
}
