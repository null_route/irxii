#include "colutil.h"
#include "ansi.h"

size_t
colutil_bytes_until(const char *s, size_t col)
{
	const char *p = s;
	size_t num_cols = 0, new_cols;
	size_t num_chr_bytes;

	/* TODO log error here */
	if (s == NULL)
		return 0;

	while (*p != '\0' && *p != '\n') {
		if (*p == '\x1B') {
			p += ansi_skip_escape(p);
			continue;
		}

		num_chr_bytes = utf8_chr_num_bytes(*p);

		if (!utf8_is_valid_chrseq(p, num_chr_bytes))
			break;

		new_cols = num_cols + utf8_chrseq_num_cols(p);

		if (new_cols >= col)
			break;

		num_cols = new_cols;
		p += num_chr_bytes;
	}

	return p - s;
}
