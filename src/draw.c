#include "draw.h"
#include "safe_free.h"
#include "strbuf.h"
#include "str.h"
#include <stdio.h>
#include <limits.h>

void
draw_set_color(enum ansi_color color)
{
	printf("\x1B[%um", color);
}

void
draw_set_pos(unsigned row, unsigned col)
{
	printf("\x1B[%u;%uH", row, col);
}

void
draw_write(const char *str)
{
	if (str)
		fputs(str, stdout);
}

void
draw_write_len(const char *str, size_t len)
{
	if (str)
		fwrite(str, len, 1, stdout);
}

#define DRAW_QUEUE_SIMPLE_FN(fn_name, esc_init)                              \
void                                                                         \
draw_##fn_name(void)                                                         \
{                                                                            \
        fwrite(esc_init, sizeof esc_init - 1, 1, stdout);                    \
}

DRAW_QUEUE_SIMPLE_FN(reset_style,          "\x1B[m")
DRAW_QUEUE_SIMPLE_FN(clear_history,        "\x1B[3J")
DRAW_QUEUE_SIMPLE_FN(clear_screen,         "\x1B[2J")
DRAW_QUEUE_SIMPLE_FN(clear_line,           "\x1B[2K")
DRAW_QUEUE_SIMPLE_FN(clear_line_to_end,    "\x1B[0K")
DRAW_QUEUE_SIMPLE_FN(clear_line_to_start,  "\x1B[1K")
DRAW_QUEUE_SIMPLE_FN(reset_col,            "\r")
DRAW_QUEUE_SIMPLE_FN(next_line_keep_col,   "\n")
DRAW_QUEUE_SIMPLE_FN(next_line,            "\r\n")
DRAW_QUEUE_SIMPLE_FN(set_secondary_screen, "\x1B[?1049h")
DRAW_QUEUE_SIMPLE_FN(set_primary_screen,   "\x1B[?1049l")
