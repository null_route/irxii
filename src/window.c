#include "window.h"
#include "term.h"
#include "str.h"
#include "safe_free.h"

void
window_init(struct window *self, const char *title)
{
	self->messages = NULL;
	self->num_messages = 0;
	window_title_bar_init(&self->title_bar, 1, title);
}

void
window_destroy(struct window *self)
{
	size_t i;

	for (i = 0; i < self->num_messages; ++i)
		message_destroy(&self->messages[i]);

	SAFE_FREE(self->messages);
	self->num_messages = 0;
	window_title_bar_destroy(&self->title_bar);
}

void
window_write(struct window *self, enum message_type type, const char *s)
{
	struct message message;

	message_init(&message, type, s);
	self->messages = (struct message*)
		mem_append(self->messages, &self->num_messages, &message, sizeof message);
}
