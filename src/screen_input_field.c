#include "screen_input_field.h"
#include "safe_free.h"
#include "term.h"
#include "str.h"
#include "draw.h"

void
screen_input_field_init(struct screen_input_field *self)
{
	str_init(&self->input);
}

void
screen_input_field_destroy(struct screen_input_field *self)
{
	str_destroy(&self->input);
}

void
screen_input_field_render(struct screen_input_field *self)
{
	draw_write(STR(self->input));
}

void
screen_input_field_append_chr(struct screen_input_field *self, char ch)
{
	str_append_chr(&self->input, ch);
}

void
screen_input_field_delete_char(struct screen_input_field *self)
{
	str_del_at(&self->input, STR_LEN(&self->input));
}

void
screen_input_field_flush(struct screen_input_field *self)
{
	str_destroy(&self->input);
	str_init(&self->input);
}

const char *
screen_input_field_get_content(struct screen_input_field *self)
{
	return STR(self->input);
}
