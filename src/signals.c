#include "signals.h"
#include "term.h"
#include "draw.h"
#include "screen_list.h"
#include <signal.h>
#include <assert.h>
#include <memory.h>

#include <stdio.h> /* temp */

static void
handle(int signal)
{
	if (signal == SIGWINCH) {
		term_update_size();
	}
}

void
signals_init(void)
{
	struct sigaction sa;

	memset(&sa, 0, sizeof sa);
	sa.sa_handler = handle;
	sigemptyset(&sa.sa_mask);
	sigaddset(&sa.sa_mask, SIGWINCH);
	sigaction(SIGWINCH, &sa, NULL);
}
