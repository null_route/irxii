#include "async_socket.h"
#include "safe_free.h"
#include "str.h"
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <errno.h>
#include <memory.h>

void
async_socket_init(struct async_socket *self)
{
	self->sock = -1;
	memset(self->buf, 0, sizeof self->buf);
	self->buf_len = 0;
}

void
async_socket_destroy(struct async_socket *self)
{
	self->sock = -1;
	memset(self->buf, 0, sizeof self->buf);
	self->buf_len = 0;
}

bool_t
async_socket_connect(struct async_socket *self, struct fd_reactor *reactor, const char *host,
	unsigned short port, short events, fd_reactor_callback_fn_t callback, void *arg)
{
	int sock;
	int flags;
	struct sockaddr_in addr;

	if (host == NULL || port == 0)
		return BOOL_FALSE;

	if (self->sock != -1) /* TODO separate errno */
		return BOOL_FALSE;

	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket() failed");
		return BOOL_FALSE;
	}

	flags = fcntl(sock, F_GETFL, 0);

	if (fcntl(sock, F_SETFL, flags | O_NONBLOCK)) {
		perror("fcntl() failed");
		return BOOL_FALSE;
	}

	memset(&addr, 0, sizeof addr);
	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = inet_addr(host);

	if (!fd_reactor_add_fd(reactor, sock, POLLIN | POLLOUT | POLLERR | POLLHUP)) {
		perror("fd_reactor_add_fd() failed"); /* TODO XXX */
		async_socket_close(self, reactor);
		return BOOL_FALSE;
	}

	if (!fd_reactor_add_callback(reactor, sock, callback, arg)) {
		perror("fd_reactor_add_callback() failed"); /* TODO XXX */
		async_socket_close(self, reactor);
		return BOOL_FALSE;
	}

	if (connect(sock, (struct sockaddr*) &addr, sizeof addr) != EINPROGRESS) {
		perror("connect() failed");
		async_socket_close(self, reactor);
		return BOOL_FALSE;
	}

	self->sock = sock;
	return BOOL_TRUE;
}

bool_t
async_socket_close(struct async_socket *self, struct fd_reactor *reactor)
{
	if (self->sock == -1)
		return BOOL_TRUE;

	fd_reactor_del_fd(reactor, self->sock);
	shutdown(self->sock, SHUT_RDWR);
	close(self->sock);
	self->sock = -1;
	return BOOL_TRUE;
}

void
async_socket_recv(struct async_socket *self)
{
	self->buf_len = recv(self->sock, &self->buf, sizeof self->buf, 0);
}
