#include <time.h>
#include "str.h"

int
timestamp_append(struct str *s, size_t *dst_len, time_t time)
{
	char ts_buf[64];
	strftime(ts_buf, sizeof ts_buf, "%H:%M ", localtime(&time));
	return str_append(s, ts_buf);
}
