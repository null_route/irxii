#include "screen_view.h"

void
screen_view_init(struct screen_view *self, size_t num_rows, size_t num_cols)
{
	self->screen = NULL;
	self->num_rows = num_rows;
	self->num_cols = num_cols;
	self->focused_window_idx = 0;
	window_view_init(&self->window_view, num_rows, num_cols);
}

void
screen_view_focus(struct screen_view *self, struct screen *screen)
{
	self->screen = screen;
}

void
screen_view_focus_window(struct screen_view *self, size_t idx)
{
	if (idx >= self->screen->num_windows)
		return;

	self->focused_window_idx = idx;
	window_view_focus(&self->window_view, &self->screen->windows[idx]);
}
