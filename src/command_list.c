#include "command_list.h"
#include "safe_free.h"
#include <string.h>
#include <ctype.h>

struct command_list command_list;

void
command_list_add_command(struct command_list *self, const char *command, command_callback_t callback)
{
	struct command object;

	command_init(&object, command, callback);

	self->commands = (struct command*)
		mem_append(self->commands, &self->num_commands, &object, sizeof object);
}

static size_t
count_ws(const char *s)
{
	const char *p = s;

	while (*p && isspace(*p))
		p++;

	return p - s;
}

static size_t
count_non_ws(const char *s)
{
	const char *p = s;

	while (*p && !isspace(*p))
		p++;

	return p - s;
}

struct command *
command_list_get_command(struct command_list *self, const char *command)
{
	size_t i;
	size_t len;

	command += count_ws(command);
	len = count_non_ws(command);

	for (i = 0; i < self->num_commands; ++i)
		if (strncmp(command, STR(self->commands[i].command), len) == 0)
			return &self->commands[i];

	return NULL;
}

bool_t
command_list_has_command(struct command_list *self, const char *command)
{
	return command_list_get_command(self, command) != NULL;
}

bool_t
command_list_del_command(struct command_list *self, const char *command)
{
	/* actually deleting command goes here; having a mem_remove_at fn would be nice :)))))) */

	return BOOL_FALSE;
}

void
command_list_process_line(struct command_list *self, const char *line)
{
	struct command *cmd;
	const char *relevant;

	/* TODO display error in client */
	if (line[0] != '/')
		return;

	relevant = &line[1];

	/* TODO display message in client */
	if ((cmd = command_list_get_command(self, relevant)) == NULL)
		return;

	cmd->callback();
}

void
command_list_init(struct command_list *self)
{
	self->commands = NULL;
	self->num_commands = 0;
}

void
command_list_destroy(struct command_list *self)
{
	size_t i;

	for (i = 0; i < self->num_commands; ++i)
		command_destroy(&self->commands[i]);

	SAFE_FREE(self->commands);
	self->num_commands = 0;
}
