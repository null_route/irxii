#include "command_builtins.h"
#include "command.h"
#include "screen_list.h"
#include "irc_server_list.h"
#include "fd_reactor.h"
#include "irc_window_map.h"
#include "window.h"
#include <stdlib.h>
#include <stdio.h>

static void
handle_quit(void)
{
	exit(0);
}

static void
handle_connect(void)
{
}

static void
handle_clear(void)
{
}

void
command_builtins_add(struct command_list *list)
{
	command_list_add_command(list, "quit", handle_quit);
	command_list_add_command(list, "clear", handle_clear);
	command_list_add_command(list, "connect", handle_connect);
}
