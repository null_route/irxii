#include "command.h"
#include "safe_free.h"

void
command_init(struct command *self, const char *command, command_callback_t callback)
{
	str_init_with(&self->command, command);
	self->callback = callback;
}

void
command_destroy(struct command *self)
{
	str_destroy(&self->command);
	self->callback = NULL;
}
