#include "screen.h"
#include "startup_logo.h"
#include "safe_free.h"

void
screen_init(struct screen *self)
{
	self->num_windows = 0;
	self->windows = NULL;

	screen_input_field_init(&self->input_field);
	screen_status_bar_init(&self->status_bar, 1, "irxii status bar");
}

void
screen_destroy(struct screen *self)
{
	size_t i;

	for (i = 0; i < self->num_windows; ++i)
		window_destroy(&self->windows[i]);

	SAFE_FREE(self->windows);
	self->num_windows = 0;
	screen_input_field_destroy(&self->input_field);
	screen_status_bar_destroy(&self->status_bar);
}

struct window *
screen_add_window(struct screen *self, const char *title)
{
	struct window window;

	window_init(&window, title);

	self->windows = (struct window*)
		mem_append(self->windows, &self->num_windows, &window, sizeof window);
	
	/* TODO error check here */
	return &self->windows[self->num_windows - 1];
}

#if 0
void
screen_render(struct screen *self)
{
	struct window *focused;

	if ((focused = screen_get_focused_window(self)) == NULL)
		return;

	window_render(focused);
	screen_status_bar_render(&self->status_bar);
	screen_input_field_render(&self->input_field);
}
#endif

bool_t
screen_delete_window(struct screen *self, size_t i)
{
	if (i == 0) {
		/* error message about deleting the primary window goes here */
		return BOOL_FALSE;
	}

	if (i >= self->num_windows)
		return BOOL_FALSE;

	/* actually deleting window goes here; having a mem_remove_at fn would be nice :)))))) */
	return BOOL_TRUE;
}
