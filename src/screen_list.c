#include "screen_list.h"
#include "safe_free.h"
#include "term.h"
#include "array.h"
#include "startup_logo.h"

struct screen_list screen_list;

static void
internal_destructor(struct screen_list *self)
{
	size_t i;

	for (i = 0; i < self->size; ++i)
		screen_destroy(&self->screens[i]);
}

ARRAY_TEMPLATE_HEADER(
	struct screen_list,
	internal,
	screens,
	size,
	reserve,
	struct screen,
	ARRAY_NULL_INIT_ARGS,
	ARRAY_STATIC
)

ARRAY_TEMPLATE_CODE(
	struct screen_list,
	internal,
	screens,
	size,
	reserve,
	struct screen,
	ARRAY_NULL_INIT_ARGS,
	ARRAY_STATIC,
	internal_destructor,
	ARRAY_NULL_DESTRUCTOR
)

size_t
screen_list_get_num_screens(struct screen_list *self)
{
	return self->size;
}

struct screen *
screen_list_screen_list(struct screen_list *self)
{
	return self->screens;
}

struct screen *
screen_list_get_focused_screen(struct screen_list *self)
{
	if (self->size == 0)
		return NULL;

	return &self->screens[self->focused];
}

struct screen *
screen_list_add_screen(struct screen_list *self)
{
	struct screen screen;
	screen_init(&screen);

	self->screens = (struct screen*)
		mem_append(self->screens, &self->size, &screen, sizeof screen);

	return &self->screens[self->size - 1];
}

void
screen_list_set_focused_screen(struct screen_list *self, size_t index)
{
	size_t num = self->size;
	size_t max = num > 0? num - 1 : 0;

	self->focused = index > max ? max : index;
}

void
screen_list_init(struct screen_list *self)
{
	internal_init(self);
	self->focused = 0;
}

void
screen_list_destroy(struct screen_list *self)
{
	internal_destroy(self);
	self->focused = 0;
}

bool_t
screen_list_delete_screen(struct screen_list *self, size_t i)
{
	if (i == 0) {
		/* error message about deleting the primary screen goes here */
		return BOOL_FALSE;
	}

	if (i >= self->size)
		return BOOL_FALSE;

	/* actually deleting screen goes here; having a mem_remove_at fn would be nice :)))))) */
	return BOOL_TRUE;
}
