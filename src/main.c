#include "term.h"
#include "screen_list.h"
#include "fd_reactor.h"
#include "draw.h"
#include "command.h"
#include "command_builtins.h"
#include "screen_view.h"
#include "irc_server_list.h"
#include "irc_window_map.h"
#include "startup_logo.h"
#include "put.h"
#include "input.h"
#include "signals.h"
#include "str.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

static void
create_primary_screen(void)
{
	struct window *window;
	struct screen *screen;

	screen_list_init(&screen_list);

	screen = screen_list_add_screen(&screen_list);
	assert(screen != NULL);
	window = screen_add_window(screen, "IrxII - smash forehead into keyboard to continue");
	assert(window != NULL);
	startup_logo_write(window);
}

static void
init_builtin_commands(void)
{
	command_list_init(&command_list);
	command_builtins_add(&command_list);
}

static void
atexit_destroy(void)
{
	draw_set_primary_screen();
	irc_server_list_destroy(&irc_server_list);
	screen_list_destroy(&screen_list);
	command_list_destroy(&command_list);
	irc_window_map_destroy(&irc_window_map);
	fd_reactor_destroy(&fd_reactor);
	term_reset();
}

int
main(int argc, char **argv)
{
	size_t num_rows;
	size_t num_cols;

	if (term_init() != 1) {
		fputs("term_init failed - is stdin not a tty?", stderr);
		exit(1);
	}

	num_rows = term_get_num_rows();
	num_cols = term_get_num_cols();

	screen_view_init(&screen_view, num_rows, num_cols);
	fd_reactor_init(&fd_reactor);
	create_primary_screen();

	init_builtin_commands();
	irc_server_list_init(&irc_server_list);
	irc_window_map_init(&irc_window_map);

	signals_init();

	draw_set_secondary_screen();
	input_init();

	atexit(atexit_destroy);

	while (1) {
		fd_reactor_update(&fd_reactor);
	}

	return 0;
}
