#include "input.h"
#include "screen_list.h"
#include "command_list.h"
#include "fd_reactor.h"
#include "put.h"
#include "safe_free.h"
#include "ansi.h"
#include "screen_list.h"
#include <unistd.h>
#include <memory.h>
#include <stdio.h>

void
input_update(short events, void *arg)
{
	(void) getchar();
#if 0
	int ch = getchar();
	struct screen *screen;
	struct screen_input_field *field;

	if ((screen = screen_list_get_focused_screen(&screen_list)) == NULL)
		return;

	if (ch == '\x1B') {
		return;
	}

	if (ch == '\r') { /* return */
		process_input(screen_input_field_get_content(field));
		screen_input_field_flush(field);
		return;
	}

	if (ch == 0x7F) { /* backspace */
		screen_input_field_delete_char(field);
		return;
	}

	if (ch < 0x20 || ch == 0x7F)
		return;
#endif
}

void
input_init(void)
{
	fd_reactor_add_entry(&fd_reactor, STDIN_FILENO, POLLIN | POLLERR | POLLHUP, input_update, NULL);
}
