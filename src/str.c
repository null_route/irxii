#include "str.h"
#include "bool.h"
#include "utf8.h"
#include <wchar.h>

ARRAY_TEMPLATE_HEADER(
	struct str,
	str_internal,
	data,
	size,
	reserved,
	char,
	ARRAY_NULL_INIT_ARGS,
	ARRAY_STATIC
)

ARRAY_TEMPLATE_CODE(
	struct str,
	str_internal,
	data,
	size,
	reserved,
	char,
	ARRAY_NULL_INIT_ARGS,
	ARRAY_STATIC,
	ARRAY_NULL_CONSTRUCTOR,
	ARRAY_NULL_DESTRUCTOR
)

int
str_init(struct str *self)
{ 
	int res;

	if ((res = str_internal_init(self)) != 0)
		return res;

	if ((res = str_internal_append(self, '\0')) != 0)
		return res;

	return 0;
}

int
str_init_with(struct str *self, const char *s)
{
	int res;

	if ((res = str_init(self)) != 0)
		return res;

	if ((res = str_append(self, s)) != 0)
		return res;

	return 0;
}

void
str_destroy(struct str *self)
{
	str_internal_destroy(self);
}

int
str_append_chr(struct str *self, char ch)
{
	int res;

	if ((res = str_internal_insert(self, STR_LEN(self), ch)) != 0)
		return res;

	return 0;
}

int
str_append(struct str *self, const char *s)
{
	int res;
	size_t len = strlen(s);

	if ((res = str_internal_insert_array(self, STR_LEN(self), s, len)) != 0)
		return res;

	return 0;
}

int
str_append_len(struct str *self, const char *s, size_t len)
{
	int res;

	if ((res = str_internal_insert_array(self, STR_LEN(self), s, len)) != 0)
		return res;

	return 0;
}

void
str_del_at(struct str *self, size_t idx)
{
}

size_t
str_len(struct str *self)
{
	return self->size > 0? STR_LEN(self) : 0;
}
